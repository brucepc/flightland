(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .state('new', {
                url: 'novo',
                templateUrl: 'app/landing/landing.form.html',
                controller: 'LandingFormController',
                controllerAs: 'LandingForm',
                parent: 'main'
            })
            .state('flightland', {
                url: 'land',
                templateUrl: 'app/landing/landing.html',
                controller: 'LandingController',
                controllerAs: 'Landing',
                parent: 'main'
            });

        $urlRouterProvider.otherwise('/');
    }

})();
