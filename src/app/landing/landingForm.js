/**
 * Created by brucecarvalho on 17/08/16.
 */
(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .controller('LandingFormController', LandingFormCtrl);

    /** @ngInject */
    function LandingFormCtrl($scope, dataStorage, moment, $log, $mdPanel) {
        var now = moment(),
            itemNumberCtrl = 0,
            defaultItem = {
                number: itemNumberCtrl,
                codigo: itemNumberCtrl,
                hourBuffer: 6,
                minuteBuffer: 0
            };

        function resetItem() {
            $scope.item = null;
            $scope.item = angular.copy(defaultItem, $scope.item);
        }

        dataStorage.count({}, function (err, count) {
            resetItem();
            if (err) {
                $log.error(err);
            } else {
                itemNumberCtrl = count + 1;
                $scope.item.number = itemNumberCtrl;
                $scope.item.codigo = itemNumberCtrl + now.format('W');
            }
        });


        $scope.save = function (form) {
            if (form.$valid) {
                moment($scope.item.data, 'DD/MM/YYYY').add($scope.item.hora);
                dataStorage.insert($scope.item, function (err) {
                    if (err) {
                        $log.error(err);
                    } else {
                        itemNumberCtrl++;
                        resetItem();
                        $scope.item.number = itemNumberCtrl;
                        $scope.item.codigo = itemNumberCtrl + now.format('W');
                        successPanel();
                    }
                });
            } else {
                $log.debug("erro no formulario!");
            }
        };

        function successPanel() {
            var panelPosition = $mdPanel.newPanelPosition()
                    .absolute()
                    .center(),
                config = {
                    position: panelPosition,
                    trapFocus: true,
                    attachTo: angular.element(document.body),
                    template: '<div><h3>Salvo com sucesso!</h3></div>',
                    clickOutsideToClose: true,
                    escapeToClose: true,
                    focusOnOpen: true
                };

            $mdPanel.create(config).open();
        }

        ///WATCHERS
        $scope.$watch('item.data', function (n, o) {
            if (n != o) {
                $scope.item.week = moment(n).format('W');
            }
        });

        $scope.$watch('item.week', function (n, o) {
            if (n != o) {
                $scope.item.codigo = itemNumberCtrl + $scope.item.week;
            }
        });

        $scope.$watch('item.hora', function (n, o) {
            if (n != o) {
                $scope.item.meta = moment(n).add(6, 'h').format('HH:mm');
            }
        });

    }
})();
