/**
 * Created by brucecarvalho on 15/08/16.
 */
(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .controller('LandingController', LandingController);

    /** @ngInject */
    function LandingController(dataStorage, moment, $log, $scope) {
        var vm = this;
        vm.labelCountdown = 'Tempo estimado para o calço';
        vm.getCountdown = function (item) {
            return moment(item.data).add(item.hora).toNow();
        };

        vm.find = function (doc) {
            dataStorage.find(doc, function (err, docs) {
                if (err) {
                    $log.error(err);
                } else {
                    $scope.flights = docs;
                    $log.debug(docs);
                }
                $scope.$apply();
            });
        };

        vm.delete = function (id) {
            dataStorage.remove({'_id': id}, {}, function (err, num) {
                if (err) {
                    $log.error(err);
                } else {
                    $log.debug(num);
                }
            });
            vm.find($scope.filtro);
        };

        vm.getMode = function (item) {
            if (item.hora > item.horaCalco) {
                return 'indeterminate';
            } else {
                return 'determinate';
            }
        };

        vm.getTotalTime = function (item) {
            var hora = moment(item.hora).format('x'),
                now = moment().format('x'),
                percentage;

            switch (true) {
                case item.calco: {
                    percentage = (now / moment(item.horaCalco).format('x')) * 100;
                    break;
                }
                case now <= hora: {
                    percentage = (now / hora) * 100;
                    break;
                }
                default: {
                    percentage = 0;
                }
            }
            $log.debug(percentage);
            return percentage;
        };

        vm.currentNavItem = 'flightland';
        $scope.flights = [];
        //     [
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'Caraca',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     },
        //     {
        //         item: '1',
        //         voo: 'JJASFDD15',
        //         status: 'STATUS 1',
        //         horaCalco: '13:53',
        //         meta: '19:53',
        //         tempoRestante: '50min'
        //     }
        // ];
        vm.find({});
    }
})();
