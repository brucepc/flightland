(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .config(config);
    /** @ngInject */
    function config($logProvider, toastrConfig, $mdThemingProvider, $mdIconProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // setting material theme
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('blue-grey');

        $mdIconProvider
            .defaultIconSet('img/icons/sets/core-icons.svg', 24);

        // Set options third-party lib
        toastrConfig.allowHtml = true;
        toastrConfig.timeOut = 3000;
        toastrConfig.positionClass = 'toast-top-right';
        toastrConfig.preventDuplicates = true;
        toastrConfig.progressBar = true;

    }

})();
