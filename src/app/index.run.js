(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {
        // amMoment.changeLocale('pt-br');
        $log.debug('runBlock end');
    }

})();
