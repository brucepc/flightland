(function () {
    'use strict';
    angular
        .module('landingCountdown',
            ['ngAnimate',
                'ngCookies',
                'ngSanitize',
                'ngMessages',
                'ngAria',
                'ngMaterial',
                'mdPickers',
                'restangular',
                'angularMoment',
                'ui.router',
                'toastr']);
})();
