(function () {
    'use strict';

    angular
        .module('landingCountdown')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($state) {
        var main = this;
        $state.go('flightland');
        main.toolBarGoBack = false;
        main.add = function () {
            $state.go('new');
            main.toolBarGoBack = true;
        };
        main.goBack = function () {
            $state.go('flightland');
            main.toolBarGoBack = false;
        }
    }
})();
