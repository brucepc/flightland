/* global malarkey:false, moment:false */
(function () {
    'use strict';
    var nedb = new Nedb('Flights');
    nedb.loadDatabase();
    angular
        .module('landingCountdown')
        .constant('malarkey', malarkey)
        .constant('moment', moment)
        .constant('dataStorage', nedb);

})();
